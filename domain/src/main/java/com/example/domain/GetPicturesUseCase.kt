package com.example.domain

import com.example.data.PictureRepository
import com.example.data.model.PictureModel
import javax.inject.Inject

class GetPicturesUseCase @Inject constructor(private val repository: PictureRepository) {
    suspend operator fun invoke(): List<PictureModel> = repository.getAllPictures()
}