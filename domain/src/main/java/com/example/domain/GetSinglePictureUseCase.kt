package com.example.domain

import com.example.data.PictureRepository
import com.example.data.model.PictureModel
import javax.inject.Inject

class GetSinglePictureUseCase @Inject constructor(private val repository: PictureRepository) {
    suspend operator fun invoke(int: Int): PictureModel = repository.getOnePicture(int)
}