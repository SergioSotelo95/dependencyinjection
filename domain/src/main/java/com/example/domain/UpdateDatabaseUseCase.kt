package com.example.domain

import com.example.data.PictureRepository
import com.example.data.model.PictureModel
import javax.inject.Inject

class UpdateDatabaseUseCase @Inject constructor(private val repository: PictureRepository) {
    suspend operator fun invoke(listOfPictures: List<PictureModel>) =
        repository.updateDatabase(listOfPictures)
}