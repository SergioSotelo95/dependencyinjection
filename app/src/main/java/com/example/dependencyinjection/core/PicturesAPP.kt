package com.example.dependencyinjection.core

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PicturesAPP : Application()