package com.example.dependencyinjection.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.data.model.PictureModel
import com.example.domain.GetSinglePictureUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SinglePictureViewModel @Inject constructor(
    private val getSinglePictureUseCase: GetSinglePictureUseCase
) : ViewModel() {
    suspend fun showPicture(id: Int): PictureModel = getSinglePictureUseCase(id)
}