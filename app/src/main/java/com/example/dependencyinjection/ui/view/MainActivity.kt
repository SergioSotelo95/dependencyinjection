package com.example.dependencyinjection.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dependencyinjection.databinding.ActivityMainBinding
import com.example.dependencyinjection.ui.adapter.PictureListAdapter
import com.example.dependencyinjection.ui.viewmodel.PictureListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    val viewModel: PictureListViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()

        viewModel.pictureListModel.observe(this) {
            (binding.RV.adapter as PictureListAdapter).submitList(it)
        }
        viewModel.isLoading.observe(this) {
            binding.swipeRefreshLayout.isRefreshing = it
        }
        binding.swipeRefreshLayout.setOnRefreshListener { viewModel.updateResults() }

    }

    private fun setupRecyclerView() = binding.RV.apply {
        adapter = PictureListAdapter()
        layoutManager = LinearLayoutManager(this@MainActivity)
    }

}