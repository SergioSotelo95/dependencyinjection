package com.example.dependencyinjection.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.example.dependencyinjection.R
import com.example.dependencyinjection.databinding.ActivityImageBinding
import com.example.dependencyinjection.ui.viewmodel.SinglePictureViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ImageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityImageBinding
    private val imageViewModel: SinglePictureViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityImageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launch {
            val pictureObject = imageViewModel.showPicture(intent.getIntExtra("id", 2))
            Glide.with(applicationContext).load(pictureObject.url + ".png")
                .placeholder(R.drawable.error).into(binding.Image)
        }

    }
}