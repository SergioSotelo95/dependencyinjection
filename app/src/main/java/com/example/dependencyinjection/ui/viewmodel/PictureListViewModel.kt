package com.example.dependencyinjection.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.data.model.PictureModel
import com.example.domain.GetPicturesUseCase
import com.example.domain.UpdateDatabaseUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PictureListViewModel @Inject constructor(
    application: Application,
    private val getPicturesUseCase: GetPicturesUseCase,
    private val updateDatabaseUseCase: UpdateDatabaseUseCase
) : AndroidViewModel(application) {
    val pictureListModel = MutableLiveData<List<PictureModel>>()
    val isLoading = MutableLiveData<Boolean>()

    init {
        updateResults()
    }

    fun updateResults() {
        viewModelScope.launch {
            isLoading.value = true
            val result = getPicturesUseCase()
            if (!result.isNullOrEmpty()) {
                pictureListModel.postValue(result)
                updateDatabaseUseCase(result)
            }
            isLoading.value = false
        }
    }

}