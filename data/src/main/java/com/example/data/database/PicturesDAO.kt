package com.example.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.model.PictureModel

@Dao
interface PicturesDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(picturesList: List<PictureModel>)

    @Query("SELECT * FROM PictureModel")
    fun get(): List<PictureModel>

    @Query("SELECT * FROM PictureModel WHERE id = :picId")
    fun getSinglePic(picId: Int): PictureModel
}