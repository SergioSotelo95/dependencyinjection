package com.example.data.network

import android.util.Log
import com.example.data.database.PicturesDAO
import com.example.data.model.PictureModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class UpdateDatabaseService @Inject constructor(
    private val db: PicturesDAO
) {
    suspend fun updateDB(listOfPictures: List<PictureModel>) {
        withContext(Dispatchers.IO) {
            try {
                db.insert(listOfPictures)
            } catch (e: Exception) {
                Log.e("Repository", "Error ${e.message}")
            }
        }
    }
}