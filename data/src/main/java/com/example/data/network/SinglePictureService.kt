package com.example.data.network

import com.example.data.database.PicturesDAO
import com.example.data.model.PictureModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SinglePictureService @Inject constructor(
    private val db: PicturesDAO
) {
    suspend fun getSinglePicture(id: Int): PictureModel {
        return withContext(Dispatchers.IO) {
            val responseDB = db.getSinglePic(id)
            responseDB

        }
    }
}
