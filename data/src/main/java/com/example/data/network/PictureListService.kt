package com.example.data.network

import com.example.data.database.PicturesDAO
import com.example.data.model.PictureModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PictureListService @Inject constructor(
    private val api: PictureApiClient,
    private val db: PicturesDAO
) {
    suspend fun getPictures(): List<PictureModel> {

        return withContext(Dispatchers.IO) {
            val responseDB = db.get()
            if (responseDB.isNullOrEmpty()) {
                val responseAPI = api.getCustomPictures((1..25).toMutableList())
                responseAPI.body() ?: emptyList()
            } else {
                responseDB
            }


        }
    }
}