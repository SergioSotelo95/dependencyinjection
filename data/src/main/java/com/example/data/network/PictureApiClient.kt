package com.example.data.network

import com.example.data.model.PictureModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PictureApiClient {

    @GET("/photos")
    suspend fun getCustomPictures(@Query("id") id: MutableList<Int>): Response<List<PictureModel>>

}