package com.example.data

import com.example.data.model.PictureModel
import com.example.data.network.PictureListService
import com.example.data.network.SinglePictureService
import com.example.data.network.UpdateDatabaseService
import javax.inject.Inject

class PictureRepository @Inject constructor(
    private val pictureListService: PictureListService,
    private val singlePictureService: SinglePictureService,
    private val updateDatabaseService: UpdateDatabaseService
) {
    suspend fun getAllPictures(): List<PictureModel> {
        return pictureListService.getPictures()
    }

    suspend fun getOnePicture(int: Int): PictureModel {
        return singlePictureService.getSinglePicture(int)
    }

    suspend fun updateDatabase(listOfPictures: List<PictureModel>) {
        return updateDatabaseService.updateDB(listOfPictures)
    }

}