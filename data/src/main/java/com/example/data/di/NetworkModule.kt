package com.example.data.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Singleton
    @Provides
    fun providePictureApiClient(retrofit: Retrofit): com.example.data.network.PictureApiClient {
        return retrofit.create(com.example.data.network.PictureApiClient::class.java)
    }

    @Singleton
    @Provides
    fun providePictureDataBase(@ApplicationContext context: Context) = Room
        .databaseBuilder(context.applicationContext, com.example.data.database.PicturesDataBase::class.java, "pictures")
        .build()

    @Singleton
    @Provides
    fun providePicturesDAO(db: com.example.data.database.PicturesDataBase) = db.picturesDao()
}